# Nginx_Deployment

In this project I have created a helm chart for nginx image with enabling Horizontal Pod Autoscaler then deployed into minikube cluster using terraform

## Helm repo

In this project I have used Gitlab package registery as helm repo following is the command for adding gitlab project as a helm repository

##### helm repo add --username <username> --password <access_token> project-1 https://gitlab.example.com/api/v4/projects/<project_id>/packages/helm/<channel>
