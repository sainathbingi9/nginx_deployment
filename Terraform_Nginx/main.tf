provider "helm" {
  kubernetes {
    config_path = "~/.kube/config"
    config_context_cluster = "minikube"
  }
}

resource "helm_release" "test" {
  name       = var.release_name
  repository = var.repository_url
  chart      = var.chart_name
  repository_username = var.repository_username 
  repository_password = var.repository_password
}


