variable "release_name"{
    type = string
    default = "nginx"
}

variable "repository_url"{
    type = string
    default = "https://gitlab.com/api/v4/projects/29805526/packages/helm/stable"
}

variable "chart_name"{
    type = string
    default = "sample_nginx"
}

variable "repository_username"{
    type = string
    default = "sainathbingi9"
}

variable "repository_password"{
    type = string
    default = "<access-token>"
}
